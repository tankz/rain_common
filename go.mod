module gitee.com/tankz/rain_common

go 1.16

require (
	github.com/go-redis/redis/v8 v8.11.4
	github.com/hashicorp/consul/api v1.12.0
	github.com/jordan-wright/email v4.0.1-0.20210109023952-943e75fe5223+incompatible
	github.com/minio/minio-go/v7 v7.0.21
	github.com/olivere/elastic/v7 v7.0.31
	go.mongodb.org/mongo-driver v1.8.3 // indirect
	gorm.io/driver/mysql v1.2.3
	gorm.io/gorm v1.22.5
)
