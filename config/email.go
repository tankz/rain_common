package config

type Email struct {
	Addr     string `toml:"addr"`
	Host     string `toml:"host"`
	Count    int    `toml:"count"`
	Identity string `toml:"identity"`
	Username string `toml:"username"`
	Password string `toml:"password"`
}
