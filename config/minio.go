package config

type Minio struct {
	Host            string `toml:"host"`
	Port            int    `toml:"port"`
	AccessKeyID     string `toml:"accessKeyID"`
	SecretAccessKey string `toml:"secretAccessKey"`
	UseSSL          bool   `toml:"useSSL"`
}
