package config

type Consul struct {
	Host string `toml:"host"`
	Port int    `toml:"port"`
}
