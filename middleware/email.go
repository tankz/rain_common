package middleware

import (
	"gitee.com/tankz/rain_common/config"
	"github.com/jordan-wright/email"
	"log"
	"net/smtp"
)

func NewEmail(conf config.Email) *email.Pool {
	pool, err := email.NewPool(
		conf.Addr,
		conf.Count,
		smtp.PlainAuth(conf.Identity, conf.Username, conf.Password, conf.Host),
	)
	if err != nil {
		log.Fatalln(err)
	}
	return pool
}
