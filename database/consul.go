package database

import (
	"fmt"
	"gitee.com/tankz/rain_common/config"
	"github.com/hashicorp/consul/api"
	"log"
)

func NewConsul(conf config.Consul) *api.Client {
	config := api.DefaultConfig()
	config.Address = fmt.Sprintf("%s:%d", conf.Host, conf.Port)
	client, err := api.NewClient(config)
	if err != nil {
		log.Fatalln(err)
	}
	return client
}
