package database

import (
	"fmt"
	"gitee.com/tankz/rain_common/config"
	"github.com/olivere/elastic/v7"
	"log"
)

func NewElasticSearch(conf config.ElasticSearch) *elastic.Client {
	url := fmt.Sprintf("http://%s:%d", conf.Host, conf.Port)
	client, err := elastic.NewClient(
		elastic.SetURL(url),
		elastic.SetBasicAuth(conf.User, conf.Password),
		elastic.SetSniff(false),
	)
	if err != nil {
		log.Fatalln(err)
	}
	return client
}