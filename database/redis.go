package database

import (
	"context"
	"fmt"
	"gitee.com/tankz/rain_common/config"
	"github.com/go-redis/redis/v8"
	"log"
	"time"
)

func NewRedis(conf config.Redis) *redis.Client {
	addr := fmt.Sprintf("%s:%d", conf.Host, conf.Port)
	rdb := redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: conf.Password, // no password set
		DB:       conf.DB,       // use default DB
	})
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	_, err := rdb.Ping(ctx).Result()
	if err != nil {
		log.Fatalln(err)
	}
	return rdb
}
