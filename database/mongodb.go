package database

import (
	"context"
	"fmt"
	"gitee.com/tankz/rain_common/config"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

func NewMongoDB(conf config.MongoDB) *mongo.Client {
	uri := fmt.Sprintf("mongodb://%s:%s@%s:%d", conf.User, conf.Password, conf.Host, conf.Port)
	client, err := mongo.Connect(context.Background(), options.Client().ApplyURI(uri))
	if err != nil {
		log.Fatalln(err)
	}
	return client
}
