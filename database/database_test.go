package database

import (
	"gitee.com/tankz/rain_common/config"
	"testing"
)

func TestNewElasticSearch(t *testing.T) {
	conf := config.ElasticSearch{
		Host: "101.33.240.26",
		Port: 9200,
		User: "elastic",
		Password: "kangzhao.tan",
	}
	client := NewElasticSearch(conf)
	t.Log(client)
}

func TestNewConsul(t *testing.T) {
	conf := config.Consul{
		Host: "101.33.240.26",
		Port: 8500,
	}
	client := NewConsul(conf)
	t.Log(client)
}
