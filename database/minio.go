package database

import (
	"fmt"
	"gitee.com/tankz/rain_common/config"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"log"
)

func NewMinio(conf config.Minio) *minio.Client {
	endpoint := fmt.Sprintf("%s:%d", conf.Host, conf.Port)
	client, err := minio.New(endpoint, &minio.Options{
		Creds: credentials.NewStaticV4(conf.AccessKeyID, conf.SecretAccessKey, ""),
	})
	if err != nil {
		log.Fatalln(err)
	}
	return client
}
